﻿// This file is part of Saxoier.Utils.
// 
// Saxoier.Utils is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Saxoier.Utils is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Saxoier.Utils. If not, see <https://www.gnu.org/licenses/>.
using System;

namespace Saxoier.Utils
{
    public static class RandomExtensions
    {
        #region Static Public
        public static double NextDouble(this Random @this, double maxValue)
        {
            return @this.NextDouble(0, maxValue);
        }

        public static double NextDouble(this Random @this, double minValue, double maxValue)
        {
            return @this.NextDouble() * (maxValue - minValue) + minValue;
        }
        #endregion
    }
}