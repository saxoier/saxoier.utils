﻿// This file is part of Saxoier.Utils.
// 
// Saxoier.Utils is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Saxoier.Utils is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Saxoier.Utils. If not, see <https://www.gnu.org/licenses/>.
using System.Collections.Generic;

namespace Saxoier.Utils
{
    // ReSharper disable once InconsistentNaming
    public static class ICollectionExtensions
    {
        #region Static Public
        public static void AddRange<T>(this ICollection<T> @this, IEnumerable<T> collection)
        {
            foreach (var item in collection)
            {
                @this.Add(item);
            }
        }

        public static void Replace<T>(this ICollection<T> @this, IEnumerable<T> collection)
        {
            @this.Clear();
            @this.AddRange(collection);
        }
        #endregion
    }
}
