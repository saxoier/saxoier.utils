﻿// This file is part of Saxoier.Utils.
// 
// Saxoier.Utils is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Saxoier.Utils is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Saxoier.Utils. If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;

namespace Saxoier.Utils
{
    public static class ObservableCollectionExtensions
    {
        #region Static Private
        private static void CallOnCollectionChangedMethod<T>(ObservableCollection<T> collection, NotifyCollectionChangedEventArgs e)
        {
            var onCollectionChangedMethod = collection.GetType().GetMethod(
                "OnCollectionChanged", BindingFlags.Instance | BindingFlags.NonPublic,
                null, new[] { typeof(NotifyCollectionChangedEventArgs) }, null);
            onCollectionChangedMethod?.Invoke(collection, new object[] { e });
        }

        private static IList<T> GetItems<T>(ObservableCollection<T> collection)
        {
            var itemsProperty = collection.GetType().GetProperty("Items", BindingFlags.Instance | BindingFlags.NonPublic);
            return (IList<T>)itemsProperty?.GetGetMethod(true).Invoke(collection, null);
        }
        #endregion

        #region Static Public
        public static void AddRange<T>(this ObservableCollection<T> @this, IEnumerable<T> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException(nameof(collection));
            }

            var items = GetItems(@this);
            foreach (var item in collection)
            {
                items.Add(item);
            }
            CallOnCollectionChangedMethod(@this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public static void Replace<T>(this ObservableCollection<T> @this, IEnumerable<T> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException(nameof(collection));
            }

            var items = GetItems(@this);
            items.Clear();
            AddRange(@this, collection);
        }

        public static int RemoveAll<T>(this ObservableCollection<T> @this, Predicate<T> match)
        {
            if (match == null)
            {
                throw new ArgumentNullException(nameof(match));
            }

            var items = GetItems(@this);
            var removeditemsCount = 0;
            foreach (var item in items.ToList())
            {
                if (match(item))
                {
                    while (items.Remove(item))
                    {
                        removeditemsCount++;
                    }
                }
            }
            CallOnCollectionChangedMethod(@this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            return removeditemsCount;
        }
        #endregion
    }
}
