﻿// This file is part of Saxoier.Utils.Wpf.
// 
// Saxoier.Utils.Wpf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Saxoier.Utils.Wpf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Saxoier.Utils.Wpf. If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;

namespace Saxoier.Utils.Wpf
{
    public abstract class ViewModel<M> : ViewModel
    {
        #region Static Public
        public new static TViewModel CreateViewModel<TModel, TViewModel>(TModel model)
            where TViewModel : ViewModel<TModel>, new()
        {
            return new TViewModel()
            {
                Model = model
            };
        }

        public new static IEnumerable<TViewModel> CreateViewModels<TModel, TViewModel>(IEnumerable<TModel> models)
            where TViewModel : ViewModel<TModel>, new()
        {
            foreach (var model in models)
            {
                yield return CreateViewModel<TModel, TViewModel>(model);
            }
        }
        #endregion

        #region Fields
        private M _model;
        #endregion

        #region Properties
        protected M Model
        {
            get { return _model; }
            set { SetModel(ref _model, value); }
        }
        #endregion

        #region Private
        private void SetModel(ref M variable, M model)
        {
            var comparer = EqualityComparer<M>.Default;
            if (comparer.Equals(variable, model))
            {
                return;
            }
            variable = model;
            OnModelChanged(EventArgs.Empty);
        }
        #endregion

        #region Public
        public M GetModel()
        {
            return Model;
        }

        public void SetModel(M model)
        {
            Model = model;
        }
        #endregion

        #region Events
        protected event EventHandler ModelSet;
        #endregion

        #region Eventraiser
        protected virtual void OnModelChanged(EventArgs e)
        {
            ModelSet?.Invoke(this, e);
        }
        #endregion
    }
}
