﻿// This file is part of Saxoier.Utils.Wpf.
// 
// Saxoier.Utils.Wpf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Saxoier.Utils.Wpf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Saxoier.Utils.Wpf. If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Windows;
using System.Windows.Input;

namespace Saxoier.Utils.Wpf
{
    public class CloseApplicationCommand : ICommand
    {
        #region Public
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            Application.Current.MainWindow?.Close();
        }
        #endregion

        #region Events
        public event EventHandler CanExecuteChanged;
        #endregion

        #region Eventraiser
        protected virtual void OnCanExecuteChanged(EventArgs e)
        {
            CanExecuteChanged?.Invoke(this, e);
        }
        #endregion
    }
}
