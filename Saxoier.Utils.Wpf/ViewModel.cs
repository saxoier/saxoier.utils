﻿// This file is part of Saxoier.Utils.Wpf.
// 
// Saxoier.Utils.Wpf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Saxoier.Utils.Wpf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Saxoier.Utils.Wpf. If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Saxoier.Utils.Wpf
{
    public abstract class ViewModel : INotifyDataErrorInfo, INotifyPropertyChanged, IDisposable
    {
        #region Static Public
        public static TViewModel CreateViewModel<TModel, TViewModel>(TModel model)
            where TViewModel : ViewModel<TModel>, new()
        {
            return ViewModel<TModel>.CreateViewModel<TModel, TViewModel>(model);
        }

        public static IEnumerable<TViewModel> CreateViewModels<TModel, TViewModel>(IEnumerable<TModel> models)
            where TViewModel : ViewModel<TModel>, new()
        {
            return ViewModel<TModel>.CreateViewModels<TModel, TViewModel>(models);
        }
        #endregion

        #region Properties
        private IDictionary<string, IList<object>> DataErrors { get; }
        private IDictionary<string, object> Values { get; }

        public bool HasErrors
        {
            get { return GetValue<bool>(); }
            private set { SetValue(value); }
        }
        #endregion

        #region Constructor
        protected ViewModel()
        {
            DataErrors = new Dictionary<string, IList<object>>();
            Values = new Dictionary<string, object>();
        }
        #endregion

        #region Protected
        protected void AddError(object value, [CallerMemberName] string propertyName = "")
        {
            if (!DataErrors.ContainsKey(propertyName))
            {
                DataErrors[propertyName] = new List<object>();
            }

            DataErrors[propertyName].Add(value);
            HasErrors = true;
            OnErrorsChanged(new DataErrorsChangedEventArgs(propertyName));
        }

        protected T GetValue<T>([CallerMemberName] string propertyName = "")
        {
            // wenn der Wert noch nicht vorkommt, dann nimm den Standardwert
            return Values.ContainsKey(propertyName) ? (T)Values[propertyName] : default(T);
        }

        protected void SetValue<T>(T value, [CallerMemberName] string propertyName = "")
        {
            // oder wenn ein Wert vorhanden ist und dieser identlich zum gespeichrten Wert ist
            // dann ignoriere alles
            if (Values.ContainsKey(propertyName) &&
                EqualityComparer<T>.Default.Equals((T)Values[propertyName], value))
            {
                return;
            }

            Values[propertyName] = value;
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void Dispose(bool disposing)
        {

        }
        #endregion

        #region Public
        public IEnumerable GetErrors(string propertyName)
        {
            return DataErrors.ContainsKey(propertyName) ? DataErrors[propertyName] : null;
        }

        public void Dispose()
        {
            // https://stackoverflow.com/a/151244
            // If the class implementing IDisposable is not sealed, then it
            // should include the call to GC.SuppressFinalize(this) even if it
            // does not include a user-defined finalizer. This is necessary to
            // ensure proper semantics for derived types that add a user-
            // defined finalizer but only override the protected Dispose(bool)
            // method.

            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Events
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Eventraiser
        protected virtual void OnErrorsChanged(DataErrorsChangedEventArgs e)
        {
            ErrorsChanged?.Invoke(this, e);
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }
        #endregion
    }
}
